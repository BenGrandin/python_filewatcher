"""
File Watcher
"""
import time
from src import watch
from connectors import BDD_sqlite3


def init(cmd_args):
    print("File Watcher launch")

    BDD_sqlite3.init()
    watch.init(cmd_args)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument("-d", "--directory", help="Name of the repertory")
    parser.add_argument("-i", "--interval",
                        help="Number of secondes of the interval")
    parser.add_argument("-c", "--config",
                        help="Name of the configuration file")
    parser.add_argument("-l", "--logging", help="Name of the logging file")
    parser.add_argument("-p", "--pattern",
                        help="Name of pattern u wanna watch")

    args_from_cmd_line = parser.parse_args()

    init(args_from_cmd_line)
