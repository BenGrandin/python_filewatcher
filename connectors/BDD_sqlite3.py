import sqlite3


def init():
    conn = sqlite3.connect('example.db')

    c = conn.cursor()

    # Create table
    c.execute(
        f"CREATE TABLE IF NOT EXISTS logs (text text)"
    )
    conn.commit()
    conn.close()


def select(where=None, table="logs"):
    """Action into a chosen table
    Args:
        table: the chosen table
        where: an array with attribute and value wanted like [attribute,value]
    """
    conn = sqlite3.connect('example.db')

    c = conn.cursor()
    if where:
        c.execute(f"SELECT * FROM {table} WHERE {where[0]} = ?", (where[1],))
    else:
        c.execute(f"SELECT * FROM {table}")

    return c.fetchall()


def insert(text, table="logs"):
    """Action into a chosen table
    Args:
        table: the chosen table
        text: the values you want to insert
    """
    conn = sqlite3.connect('example.db')

    c = conn.cursor()

    req = f"INSERT INTO {table} VALUES (?);"
    c.execute(req, (text,))

    # Save (commit) the changes
    conn.commit()
    conn.close()


def update(attribute, values, where, table="logs"):
    """Action into a chosen table
    Args:
        table: the chosen table
        attribute: the attribute you want to change
        values: the values you want to insert
        where: the condition where you want these chance to apply
    """
    conn = sqlite3.connect('example.db')

    c = conn.cursor()

    c.execute(
        f"UPDATE {table} "
        f"SET {attribute} = {values}"
        f" WHERE {where};")

    # Save (commit) the changes
    conn.commit()
    conn.close()


def delete(where, table="logs", all=False):
    """Delete lines into a chosen table
    Args:
        table: the chosen table
        where: the condition where you want these chance to apply
        all: set to True to erase all data on the table
    """
    conn = sqlite3.connect('example.db')

    c = conn.cursor()

    if all:
        c.execute(
            f"DELETE FROM {table}")
    else:
        c.execute(
            f"DELETE FROM {table} WHERE text = ?", (where,)
        )

    # Save (commit) the changes
    conn.commit()
    conn.close()
